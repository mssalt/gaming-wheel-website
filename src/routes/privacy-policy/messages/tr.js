export const messages = {
  privacyPolicy: `
    <h1>
        <strong>Gizlilik Politikası</strong>
    </h1>
    <p>mssalt, Gaming Wheel uygulamasını bir Ticari uygulama olarak oluşturdu. Bu HİZMET, mssalt tarafından
        sağlanmaktadır ve olduğu gibi kullanılması amaçlanmıştır.</p>
    <p>Bu sayfa, herhangi biri Hizmetimi kullanmaya karar verirse, ziyaretçileri Kişisel Bilgilerin toplanması,
        kullanılması ve ifşa edilmesiyle ilgili politikalarım hakkında bilgilendirmek için kullanılır.</p>
    <p>Hizmetimi kullanmayı seçerseniz, bu politikayla ilgili bilgilerin toplanmasını ve kullanılmasını kabul etmiş
        olursunuz. Topladığım Kişisel Bilgiler, Hizmeti sağlamak ve geliştirmek için kullanılır. Bilgilerinizi bu
        Gizlilik Politikasında belirtilen durumlar dışında kullanmayacağım veya kimseyle paylaşmayacağım.</p>
    <p>Bu Gizlilik Politikasında kullanılan terimler, bu Gizlilik Politikasında aksi belirtilmedikçe Gaming
        Wheel'den erişilebilen Şartlar ve Koşullarımızdaki anlamlarla aynıdır.</p>
    <h2>
        <strong>Bilgi Toplama ve Kullanma</strong>
    </h2>
    <p>Daha iyi bir deneyim için Hizmetimizi kullanırken, Hiçbiri dahil ancak bunlarla sınırlı olmamak üzere belirli
        kişisel olarak tanımlanabilir bilgileri bize vermenizi isteyebilirim. İstediğim bilgiler cihazınızda
        saklanacak ve tarafımdan hiçbir şekilde toplanmayacak.</p>
    <p>Uygulama, sizi tanımlamak için kullanılan bilgileri toplayabilecek üçüncü taraf hizmetleri kullanıyor.</p>
    <p>Uygulama tarafından kullanılan üçüncü taraf hizmet sağlayıcıların gizlilik politikasına bağlantılar:</p>
    <ul>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://www.google.com/policies/privacy/">
      Google
    </a>
  </li>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://support.google.com/admob/answer/6128543?hl=en">
      AdMob
    </a>
  </li>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://privacy.microsoft.com/en-ca/privacystatement">
      Microsoft
    </a>
  </li>
</ul>
    <h2>
        <strong>Günlük Verileri</strong>
    </h2>
    <p>Hizmetimi her kullandığınızda, uygulamada bir hata olması durumunda, telefonunuzda Günlük Verileri adı
        verilen verileri ve bilgileri (üçüncü taraf ürünler aracılığıyla) topladığımı size bildirmek isterim. Bu
        Günlük Verileri, cihazınızın İnternet Protokolü (“IP”) adresi, cihaz adı, işletim sistemi sürümü, Hizmetimi
        kullanırken uygulamanın yapılandırması, Hizmeti kullanımınızın saati ve tarihi ve diğer istatistikler gibi
        bilgileri içerebilir. .</p>
    <h2>
        <strong>Çerezler</strong>
    </h2>
    <p>Çerezler, genellikle anonim benzersiz tanımlayıcılar olarak kullanılan az miktarda veri içeren dosyalardır.
        Bunlar, ziyaret ettiğiniz web sitelerinden tarayıcınıza gönderilir ve cihazınızın dahili belleğinde
        saklanır.</p>
    <p>Bu Hizmet, bu "çerezleri" açıkça kullanmaz. Ancak uygulama, bilgi toplamak ve hizmetlerini iyileştirmek için
        "çerezler" kullanan üçüncü taraf kodlarını ve kitaplıklarını kullanabilir. Bu tanımlama bilgilerini kabul
        etme veya reddetme seçeneğine sahipsiniz ve cihazınıza bir tanımlama bilgisinin ne zaman gönderildiğini
        bilirsiniz. Tanımlama bilgilerimizi reddetmeyi seçerseniz, bu Hizmetin bazı bölümlerini
        kullanamayabilirsiniz.</p>
    <h2>
        <strong>Servis Sağlayıcılar</strong>
    </h2>
    <p>Aşağıdaki sebeplerden dolayı üçüncü taraf şirket ve bireyleri işe alabilirim:</p>
    <ul>
        <li>Hizmetimizi kolaylaştırmak için;</li>
        <li>Hizmeti bizim adımıza sağlamak için;</li>
        <li>Hizmetle ilgili hizmetleri gerçekleştirmek için; veya</li>
        <li>Hizmetimizin nasıl kullanıldığını analiz etmemize yardımcı olmak için.</li>
    </ul>
    <p>Bu Hizmetin kullanıcılarına, bu üçüncü tarafların Kişisel Bilgilerine erişimi olduğunu bildirmek istiyorum.
        Bunun nedeni ise kendilerine verilen görevleri bizim adımıza yerine getirmektir. Ancak, bilgileri ifşa
        etmemek veya başka bir amaçla kullanmamakla yükümlüdürler.</p>
    <h2>
        <strong>Güvenlik</strong>
    </h2>
    <p>Kişisel Bilgilerinizi bize sağlama konusundaki güveninize değer veriyorum, bu nedenle ticari olarak kabul
        edilebilir koruma yöntemlerini kullanmaya çalışıyoruz. Ancak internet üzerinden hiçbir aktarım yönteminin
        veya elektronik depolama yönteminin %100 güvenli ve güvenilir olmadığını ve mutlak güvenliğini garanti
        edemediğimi unutmayın.</p>
    <h2>
        <strong>Diğer Sitelere Bağlantılar</strong>
    </h2>
    <p>Bu Hizmet, diğer sitelere bağlantılar içerebilir. Bir üçüncü taraf bağlantısına tıklarsanız, o siteye
        yönlendirileceksiniz. Bu harici sitelerin benim tarafımdan işletilmediğini unutmayın. Bu nedenle, bu web
        sitelerinin Gizlilik Politikasını incelemenizi şiddetle tavsiye ederim. Herhangi bir üçüncü şahıs
        sitelerinin veya hizmetlerinin içeriği, gizlilik politikaları veya uygulamaları üzerinde hiçbir kontrole
        sahip değilim ve hiçbir sorumluluk kabul etmiyorum. </p>
    <h2>
        <strong>Çocukların Gizliliği</strong>
    </h2>
    <p>Bu Hizmetler, 13 yaşın altındaki hiç kimseye hitap etmemektedir. 13 yaşın altındaki çocuklardan kişisel
        olarak tanımlanabilir bilgileri bilerek toplamıyorum. 13 yaşından küçük bir çocuğun bana kişisel bilgiler
        sağladığını fark etmem durumunda, bunu sunucularımızdan derhal silerim. Bir ebeveyn veya vasiyseniz ve
        çocuğunuzun bize kişisel bilgiler sağladığının farkındaysanız, gerekli işlemleri yapabilmem için lütfen
        benimle iletişime geçin.</p>
    <h2>
        <strong>Bu Gizlilik Politikasındaki Değişiklikler</strong>
    </h2>
    <p>Gizlilik Politikamızı zaman zaman güncelleyebilirim. Bu nedenle, herhangi bir değişiklik için bu sayfayı
        periyodik olarak gözden geçirmeniz tavsiye edilir. Yeni Gizlilik Politikasını bu sayfada yayınlayarak
        herhangi bir değişikliği size bildireceğim.</p>
    <p>Bu politika 30 Ekim 2023 tarihinden itibaren geçerlidir.</p>
    <h2>
        <strong>Bana Ulaşın</strong>
    </h2>
    <p>Gizlilik Politikamla ilgili herhangi bir sorunuz veya öneriniz varsa,
    <a href="mailto:mss1451@gmail.com">mss1451@gmail.com</a> adresinden benimle iletişime geçmekten çekinmeyin.
    </p>
`}
