export const messages = {
  privacyPolicy: `
        <h1>
            <strong>Política de Privacidade</strong>
        </h1>
        <p>mssalt criou o aplicativo Gaming Wheel como um aplicativo comercial. Este SERVIÇO é fornecido por mssalt e
            Destina-se a
            usar como está.</p>
        <p>Esta página é usada para informar os visitantes sobre minhas políticas de coleta, uso e
            divulgação de
            Informações pessoais se alguém decidir usar meu serviço.</p>
        <p>Se você optar por usar meu serviço, concorda com a coleta e o uso de informações em
            relação a isso
            política. As informações pessoais que eu coleto são usadas para fornecer e melhorar o
            Serviço. Eu não vou
            use ou compartilhe suas informações com qualquer pessoa, exceto conforme descrito nesta Política de Privacidade.
        </p>
        <p>Os termos usados nesta Política de Privacidade têm os mesmos significados que em nossos Termos e Condições,
            que são
            acessível no Gaming Wheel, a menos que definido de outra forma nesta Política de Privacidade.</p>
        <h2>
            <strong>Coleta e uso de informações</strong>
        </h2>
        <p>Para uma melhor experiência, ao usar nosso Serviço, posso solicitar que você nos forneça
            certo pessoalmente
            informações identificáveis, incluindo, mas não se limitando a Nenhum. As informações que eu solicito
            será retido
            no seu dispositivo e não é coletado por mim de forma alguma.</p>
        <p>O aplicativo usa serviços de terceiros que podem coletar informações usadas para identificar você.</p>
        <p>Links para a política de privacidade de provedores de serviços terceirizados usados pelo aplicativo:</p>
    <ul>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://www.google.com/policies/privacy/">
      Google
    </a>
  </li>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://support.google.com/admob/answer/6128543?hl=en">
      AdMob
    </a>
  </li>
  <li>
    <a
      rel="noreferrer noopener"
      target="_blank"
      href="https://privacy.microsoft.com/en-ca/privacystatement">
      Microsoft
    </a>
  </li>
</ul>
        <h2>
            <strong>Dados de registro</strong>
        </h2>
        <p>Quero informar que sempre que você usar meu Serviço, em caso de erro no aplicativo, eu
            coletar dados e
            informações (através de produtos de terceiros) em seu telefone chamado Log Data. Esses dados de registro podem
            incluir
            informações como o endereço do Protocolo de Internet (“IP”) do seu dispositivo, nome do dispositivo,
            versão do sistema,
            a configuração do aplicativo ao utilizar meu serviço, a hora e a data de seu uso do
            Serviço,
            e outras estatísticas.</p>
        <h2>
            <strong>Cookies</strong>
        </h2>
        <p>Cookies são arquivos com uma pequena quantidade de dados que são comumente usados como únicos anônimos
            identificadores.
            Estes são enviados para o seu navegador a partir dos sites que você visita e são armazenados no seu
            interno do dispositivo
            memória.</p>
        <p>Este Serviço não usa esses “cookies” explicitamente. No entanto, o aplicativo pode usar terceiros
            código e
            bibliotecas que usam “cookies” para coletar informações e melhorar seus serviços. Você tem a
            opção para
            aceite ou recuse esses cookies e saiba quando um cookie está sendo enviado para o seu dispositivo.
            Se você escolher
            recusar nossos cookies, talvez você não consiga usar algumas partes deste Serviço.</p>
        <h2>
            <strong>Provedores de Serviços</strong>
        </h2>
        <p>Posso contratar empresas e indivíduos terceirizados pelos seguintes motivos:</p>
        <ul>
            <li>Para facilitar nosso Serviço;</li>
            <li>Para fornecer o Serviço em nosso nome;</li>
            <li>Para realizar serviços relacionados ao Serviço; ou</li>
            <li>Para nos ajudar a analisar como nosso Serviço é usado.</li>
        </ul>
        <p>Desejo informar aos usuários deste Serviço que esses terceiros têm acesso a seus Dados Pessoais
            Informação. O motivo é realizar as tarefas atribuídas a eles em nosso nome. No entanto,
            eles são
            obrigado a não divulgar ou usar as informações para qualquer outra finalidade.</p>
        <h2>
            <strong>Segurança</strong>
        </h2>
        <p>Valorizo sua confiança em nos fornecer suas informações pessoais, por isso estamos nos esforçando para usar
            meios comercialmente aceitáveis de protegê-lo. Mas lembre-se que nenhum método de transmissão
            sobre o
            internet ou método de armazenamento eletrônico é 100% seguro e confiável, e não posso
            garantir o seu
            segurança absoluta.</p>
        <h2>
            <strong>Links para outros sites</strong>
        </h2>
        <p>Este serviço pode conter links para outros sites. Se você clicar em um link de terceiros, você
            ser
            direcionado para esse site. Observe que esses sites externos não são operados por mim. Portanto, eu
            fortemente
            aconselhá-lo a rever a Política de Privacidade desses sites. Eu não tenho controle e assumo
            não
            responsabilidade pelo conteúdo, políticas de privacidade ou práticas de quaisquer sites de terceiros ou
            Serviços.
        </p>
        <h2>
            <strong>Privacidade das crianças</strong>
        </h2>
        <p>Estes Serviços não se dirigem a menores de 13 anos. Não recolho intencionalmente
            pessoalmente
            informações identificáveis de crianças menores de 13 anos de idade. No caso eu descobrir que um
            criança
            menor de 13 anos me forneceu informações pessoais, eu as excluo imediatamente de nosso
            servidores. Se
            você é pai ou responsável e está ciente de que seu filho nos forneceu informações pessoais
            informações, entre em contato comigo para que eu possa tomar as providências necessárias.</p>
        <h2>
            <strong>Mudanças nesta Política de Privacidade</strong>
        </h2>
        <p>Eu posso atualizar nossa Política de Privacidade de tempos em tempos. Assim, você é aconselhado a rever esta
            página
            periodicamente para quaisquer alterações. Vou notificá-lo sobre quaisquer alterações, publicando o novo Privacy
            Política sobre
            esta página.</p>
        <p>Esta política entra em vigor a partir de 30 de outubro de 2023</p>
        <h2>
            <strong>Fale comigo</strong>
        </h2>
        <p>Se você tiver alguma dúvida ou sugestão sobre minha Política de Privacidade, não hesite em entrar em contato
            me em <a href="mailto:mss1451@gmail.com">mss1451@gmail.com</a>.</p> 
`}
