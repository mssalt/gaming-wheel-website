export const messages = {
  appTitle: 'Oyun Direksiyonu PC',
  homeTab: 'Ana Sayfa',
  privacyPolicyTab: 'Gizlilik Politikası',
  contactTab: 'İletişim',
  helpTab: 'Yardım',
};
