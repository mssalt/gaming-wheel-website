export const messages = {
  appTitle: "Gaming Wheel App",
  homeTab: "Home",
  privacyPolicyTab: "Privacy Policy",
  contactTab: "Contact",
  helpTab: "Help",
};
