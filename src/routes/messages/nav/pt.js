export const messages = {
  appTitle: "Roda de Jogos",
  homeTab: "Pagina inicial",
  privacyPolicyTab: "Política de Privacidade",
  contactTab: "Contato",
  helpTab: "Ajuda",
};
