export const messages = {
  intro: "Play driving games on Windows or Linux using your Android device as a steering wheel.",
  downloadFromGoogleDrive: "Download from Google Drive",
  downloadPcAppTitle: "Download PC App",
  latestVersionInfo:
    "The latest version is {{appVersion}} with network protocol version {{networkVersion}}",
  windowsInstruction1: "First, download and install the vJoy virtual joystick driver.",
  downloadVjoyLabel: "Download vJoy Driver",
  windowsInstruction2:
    "Run <strong>Configure vJoy</strong>, set the number of buttons to 28, and ensure that all axes are enabled.",
  windowsInstruction3: "Finally, download the controller program below.",
  downloadWindows64Bit: "Download 64-bit",
  downloadWindows32Bit: "Download 32-bit",
  linuxInstruction:
    'You must have the <strong>"uinput"</strong> module installed and enabled on your system. The program uses the module to create and manage virtual input devices.',
  downloadLinux64Bit: "Download 64-bit",
  sourceCodeLink: "Source code at Gitlab",
  featuresTitle: "Features",
  uiSchemaUrl: "/images/ui-schema-en.webp",
  setupTitle: "Installation and Usage",
};
