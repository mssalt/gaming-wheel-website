export const messages = {
  intro: 'Android cihazınızı kullanarak Windows veya Linux\'te sürüş oyunları oynayın.',
  downloadFromGoogleDrive: "Google Drive'dan indirin",
  downloadPcAppTitle: 'PC Uygulamasını İndir',
  latestVersionInfo: 'Son sürüm {{appVersion}} ve ağ protokolü sürümü {{networkVersion}}',
  windowsInstruction1: 'Öncelikle vJoy sanal joystick sürücüsünü indirip kurun.',
  downloadVjoyLabel: 'vJoy Sürücüsünü İndir',
  windowsInstruction2: '<strong>Configure vJoy</strong>\'u çalıştırın, düğme sayısını 28\'e ayarlayın ve tüm eksenlerin aktif olduğundan emin olun.',
  windowsInstruction3: 'Son olarak aşağıdaki kontrolcü programını indirin.',
  downloadWindows64Bit: '64-bit İndir',
  downloadWindows32Bit: '32-bit İndir',
  linuxInstruction: 'Sisteminizde <strong>"uinput"</strong> modülünün kurulu ve etkinleştirilmiş olması gerekir. Program, sanal giriş aygıtları oluşturmak ve yönetmek için bu modülü kullanır.',
  downloadLinux64Bit: '64-bit İndir',
  sourceCodeLink: 'Gitlab kaynak kodu',
  featuresTitle: 'Özellikler',
  uiSchemaUrl: '/images/ui-schema-tr.webp',
  setupTitle: 'Kurulum ve Kullanım',
};
