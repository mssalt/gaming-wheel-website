import { getUserLang } from '$lib/index';

import { messages as navEn } from './messages/nav/en';
import { messages as navTr } from './messages/nav/tr';
import { messages as navPt } from './messages/nav/pt';
import { messages as navRu } from './messages/nav/ru';

import { messages as homeEn } from './messages/home/en';
import { messages as homeTr } from './messages/home/tr';
import { messages as homePt } from './messages/home/pt';
import { messages as homeRu } from './messages/home/ru';

/** @type {import('./$types').LayoutServerLoad} */
export async function load({ request }) {
  const lang = getUserLang(request.headers);
  switch (lang) {
    case 'en':
      return { homeMessages: homeEn, navMessages: navEn };
    case 'tr':
      return { homeMessages: homeTr, navMessages: navTr };
    case 'pt':
      return { homeMessages: homePt, navMessages: navPt };
    case 'ru':
      return { homeMessages: homeRu, navMessages: navRu };
    default:
      return { homeMessages: homeEn, navMessages: navEn };
  }
}
