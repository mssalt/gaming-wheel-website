import { getUserLang } from '$lib/index';

import { messages as en } from './messages/en';
import { messages as tr } from './messages/tr';
import { messages as pt } from './messages/pt';
import { messages as ru } from './messages/ru';

/** @type {import('./$types').PageServerLoad} */
export async function load({ request }) {
  const lang = getUserLang(request.headers);
  switch (lang) {
    case 'en':
      return { messages: en };
    case 'tr':
      return { messages: tr };
    case 'pt':
      return { messages: pt };
    case 'ru':
      return { messages: ru };
    default:
      return { messages: en };
  }
}
