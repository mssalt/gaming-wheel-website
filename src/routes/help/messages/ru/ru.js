import {
   title as topicConnectMulticastTitle,
   content as topicConnectMulticastContent
} from './topics/connectMulticast';
import {
   title as topicConnectUnicastTitle,
   content as topicConnectUnicastContent
} from './topics/connectUnicast';
import {
  title as topicKeepsDisconnTitle,
  content as topicKeepsDisconnContent,
} from "./topics/keepsDisconn";
import {
   title as topicNotEnoughButtonsTitle,
   content as topicNotEnoughButtonsContent
} from './topics/notEnoughButtons';
import {
   title as topicVcr140NotFoundTitle,
   content as topicVcr140NotFoundContent
} from './topics/vcr140NotFound';
import {
   title as topicConfigFilesTitle,
   content as topicConfigFilesContent
} from './topics/configFile';
import {
   title as topicUsingShifterTitle,
   content as topicUsingShifterContent
} from './topics/usingShifter';
import {
   title as topicFixLagTitle,
   content as topicFixLagContent
} from './topics/fixLag';
import {
   title as topicUinputDeniedTitle,
   content as topicUinputDeniedContent
} from './topics/uinputDenied';
import {
   title as topicVjoyWindowsTitle,
   content as topicVjoyWindowsContent
} from './topics/vjoyWindows';
import {
   title as topicUinputLinuxTitle,
   content as topicUinputLinuxContent
} from './topics/uinputLinux';
import {
   title as topicAxisSettingsTitle,
   content as topicAxisSettingsContent
} from './topics/axisSettings';
import {
   title as topicNetProtoVerTitle,
   content as topicNetProtoVerContent
} from './topics/netProtoVer';
import {
   title as topicJoyScriptTitle,
   content as topicJoyScriptContent
} from './topics/joyScript';

export const messages = {
  foundResultLabel: "найден 1 результат",
  foundResultsLabel: "найдено {{count}} результата",
  foundNoResultLabel: "результатов не найдено",
  topicConnectMulticastTitle,
  topicConnectMulticastContent,
  topicConnectUnicastTitle,
  topicConnectUnicastContent,
  topicKeepsDisconnTitle,
  topicKeepsDisconnContent,
  topicNotEnoughButtonsTitle,
  topicNotEnoughButtonsContent,
  topicVcr140NotFoundTitle,
  topicVcr140NotFoundContent,
  topicConfigFilesTitle,
  topicConfigFilesContent,
  topicUsingShifterTitle,
  topicUsingShifterContent,
  topicFixLagTitle,
  topicFixLagContent,
  topicUinputDeniedTitle,
  topicUinputDeniedContent,
  topicVjoyWindowsTitle,
  topicVjoyWindowsContent,
  topicUinputLinuxTitle,
  topicUinputLinuxContent,
  topicAxisSettingsTitle,
  topicAxisSettingsContent,
  topicNetProtoVerTitle,
  topicNetProtoVerContent,
  topicJoyScriptTitle,
  topicJoyScriptContent,
};
