export const title = 'Исправление заиканий и лагов';
export const content = `
<p>Причин может быть много. Вот несколько вещей, которые вы можете попробовать:</p>
  <ul>
    <li>
      Ваше сетевое соединение может быть слабым или занятым. Например, кто-то еще может что-то скачивать. Одна из вещей, которую вы можете сделать в этом случае, — это создать сеть точек доступа с вашего мобильного устройства и использовать ее.
    </li>
    <li>
        Сигналы Bluetooth и WiFi могут создавать помехи
        (
          <a
            href="https://electronics.stackexchange.com/questions/466798/does-bluetooth-interfere-with-wifi"
            target="_blank"
            rel="noreferrer"
          >
            Does Bluetooth interfere with WiFi?
          </a>
        ).
        Например, если ваша Bluetooth-гарнитура подключена к телефону, вы можете наблюдать заикание на руле. В этом случае просто оставьте гарнитуру Bluetooth подключенной к компьютеру.
    </li>
     <li>
         В некоторых случаях производительность многоадресной рассылки может быть ниже, чем у одноадресной. Для подключения в режиме unicast смотрите инструкции в теме подключения. </li>
     <li>
         Если время отправки пакетов мобильным устройством слишком мало, это может вызывать периодические зависания на стороне компьютера или в сети. Попробуйте увеличить задержку отправки пакетов в сетевых настройках мобильного приложения, но это увеличит задержку. </li>
     <li>
         Номер резервного пакета определяет, сколько копий исходного пакета отправляется вместе с ним. Слишком большое увеличение этого числа может иметь негативные последствия в перегруженной сети. Сначала попробуйте уменьшить это число, а если вы не получите положительных результатов, попробуйте увеличить его. </li>
 </ul>
`;
