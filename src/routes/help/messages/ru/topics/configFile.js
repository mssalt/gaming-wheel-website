export const title = 'Конфигурация';
export const content = `
  <p>Файлы конфигурации сохраняются на языке <strong>yaml</strong>. Приложение сохраняет настройки при каждом закрытии. Однако, если приложение закрывается с ошибкой, настройки не сохраняются.</p>
  <p>Расположение папки сохранения в Windows — <i>%APPDATA%/gaming_wheel_pc</i>, а в Linux — <i>~/.config/gaming_wheel_pc</i>.</p>
`;
