export const title = 'Ошибка vcruntime140.dll не найдена';
export const content = `
<p>Возможно, в вашей системе не установлен распространяемый пакет VS 2017 C++.</p>
<p>
  Если ваша система <strong>64-битная</strong>, вы можете скачать ее отсюда:
  <a
  rel="noreferrer noopener"
  href="https://aka.ms/vs/17/release/vc_redist.x64.exe">
    https://aka.ms/vs/17/release/vc_redist.x64.exe
  </a>
</p>
<p>
  Если ваша система <strong>32-битная</strong>, вы можете скачать ее отсюда:
  <a
  rel="noreferrer noopener"
  href="https://aka.ms/vs/17/release/vc_redist.x86.exe">
    https://aka.ms/vs/17/release/vc_redist.x86.exe
  </a>
</p>
`;
