export const title = "Соединение обрывается каждые несколько минут";
export const content = `
  <p>
    По умолчанию приложение устанавливает соединение в режиме «многоадресной рассылки», но
    некоторые системы через некоторое время прекращают общение в этом режиме.
  </p>
  <p>
    Чтобы устранить эту проблему, измените настройки подключения для работы в «одноадресном» режиме.
    Вы можете посмотреть, как это сделать, здесь:
    <a href="#Подключение в одноадресном режиме">Подключение в одноадресном режиме</a>
  </p>
`;
