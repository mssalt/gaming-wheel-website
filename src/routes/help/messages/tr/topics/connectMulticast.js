export const title = 'Çok Noktaya Yayın Kipinde Bağlanma';
export const content = `
  <ol>
      <li>Mobil uygulamanın ağ ayarlarına girin.</li>
      <li>Hedef adresi 234.0.0.55 olarak ayarlayın.</li>
      <li>Hedef bağlantı noktasını 41384 olarak ayarlayın.</li>
      <li>PC uygulamasının ağ ayarlarına gidin.</li>
      <li>Bağlama adresini 234.0.0.55 olarak ayarlayın.</li>
      <li>Bağlantı noktasını 41384 olarak ayarlayın.</li>
      <li>
          Tercih edilen arayüz gelişmiş bir ayardır. Buraya <b>Ağ Arayüzleri</b> penceresinde
          gördüğünüz bir arayüz ismini girdiğiniz takdirde bağlantı sadece o ağ arayüzünde yapılır.
      </li>
      <li>
          Hedef adres, 234.0.0.0 ve 234.255.255.255 aralığında bir adrese ayarlandığı sürece çoklu
          noktaya yayın adresi olarak algılanacaktır.
      </li>
  </ol>
`;
