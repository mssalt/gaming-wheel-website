export const title = 'Vites Kullanımı';
export const content = `
  <ol>
      <li>Üzerinde sayılar bulunan dairesel bir şekle sahip olan sanal vitesi bulun.</li>
      <li>Toplamda 9 vites (7 + N + R) olduğuna dikkat edin.</li>
      <li>N vitesine geçmek için daireye dokunun.</li>
      <li>Beyaz arka plana sahip bir vitese geçmek için dairenin ortasına dokunun ve parmağınızı ona doğru kaydırın.</li>
      <li>Siyah arka plana sahip bir vitese geçmek için parmağınızı vitese doğru kaydırın ve ardından geri kaydırın.</li>
  </ol>
`;
