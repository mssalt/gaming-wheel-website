export const title = 'vcruntime140.dll Bulunamadı Hatası';
export const content = `
  <p>Sisteminizde yeniden dağıtılabilir VS 2017 C++ paketi kurulu olmayabilir.</p>
  <p>
    Sisteminiz <strong>64 bit</strong> ise şuradan indirebilirsiniz:
    <a 
    rel="noreferrer noopener"
    href="https://aka.ms/vs/17/release/vc_redist.x64.exe">
      https://aka.ms/vs/17/release/vc_redist.x64.exe
    </a>
  </p> 
  <p>
    Sisteminiz <strong>32 bit</strong> ise şuradan indirebilirsiniz:
    <a 
    rel="noreferrer noopener"
    href="https://aka.ms/vs/17/release/vc_redist.x86.exe">
      https://aka.ms/vs/17/release/vc_redist.x86.exe
    </a>
  </p> 
`;
