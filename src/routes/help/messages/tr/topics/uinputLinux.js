export const title = 'uinput (Linux)';
export const content = `
  <p>
      uinput sanal girdi cihazları oluşturmaya yarayan bir kernel modülüdür.
      Uygulama bu modül ile sanal girdi cihazları oluşturur ve bu cihazlara mobil uygulamadan gelen
      komutları gönderir. Bu modül olmadan oyun kolu, fare ve klavye özellikleri çalışmaz.
  </p>
  <p>
      uinput sisteminizde yüklü olsa bile aktif olmayabilir. Aktif etmek için alttaki komutu
      girin.
  </p>
  <code>
      $ sudo modprobe uinput
  </code>
`;
