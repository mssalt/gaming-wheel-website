export const title = 'Ağ Protokolü Sürümü';
export const content = `
  <ul>
      <li>Bu yazılım, mobil ve PC uygulamaları arasındaki iletişim için belirli bir ağ protokolü kullanır.</li>
      <li>Hem mobil hem de PC uygulamalarının birbirleriyle iletişim kurmak için aynı ağ protokolü sürümünü kullanması önemlidir.</li>
      <li>Ağ protokolü sürümleri eşleşmezse uygulamalar arasında iletişim <strong>mümkün olmaz</strong>.</li>
      <li>Bu nedenle, uygulamalar arasında sorunsuz iletişim sağlamak için lütfen hem mobil hem de PC uygulamalarının aynı ağ protokolü sürümünü kullandığından emin olun.</li>
  </ul>
`;
