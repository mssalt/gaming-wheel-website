export const title = 'Bağlantı Birkaç Dakikada Bir Kesiliyor';
export const content = `
  <p>
    Uygulama varsayılan olarak "çok noktaya yayın" kipinde bağlantı kurar ancak bazı sistemler bu
    kipte kurulan iletişimi kısa bir süre içinde sonlandırır.
  </p>
  <p>
    Bu sorunu düzeltmek için bağlantı ayarlarını "tek noktaya yayın" kipinde çalışacak şekilde
    değiştirin. Nasıl yapılacağına şuradan bakabilirsiniz:
    <a href="#Tek Noktaya Yayın Kipinde Bağlanma">Tek Noktaya Yayın Kipinde Bağlanma</a>
  </p>
`;
