export const title = 'Yapılandırma Dosyaları';
export const content = `
  <p>
      Yapılandırma dosyaları <strong>yaml</strong> dilinde kaydedilir. Uygulama her kapanışında
      ayarları kaydeder. Ancak uygulama hata vererek kapanırsa ayarlar kaydedilmez.
  </p>
  <p>
      Kayıt klasörünün Windows'taki konumu <i>%APPDATA%/gaming_wheel_pc</i>, Linux'teki konumu ise
      <i>~/.config/gaming_wheel_pc</i>'dir.
  </p>
`;
