export const title = 'vJoy (Windows)';
export const content = `
  vJoy kendisini gerçek bir oyun kolu gibi tanıtan sanal bir oyun kolu sürücüsüdür.
  Uygulama ise sadece bu sürücüye mobil uygulamadan gelen komutları gönderir. Bu sürücü olmadan
  oyun kolu özelliği çalışmaz.
`;
