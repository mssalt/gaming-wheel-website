export const title = 'Oyun Kolu Betikleme';
export const content = `
  <p>
      Betikleme gelişmiş bir özelliktir ve kullanmak için lua betik dilini bilmeyi gerektirir.
  </p>
  <p>
      Bilgisayar uygulaması bir oyun kolu cihazı tanımlamak ve onu yönetmek için betik veri klasöründeki
      <strong>default.lua</strong> dosyasını çalıştırır. Bu dosya, kullanılacak cihazın ismini, arayüzünü,
      satıcı ve ürün kimliğini, sahip olduğu girdileri vs. tanımlar ve mobil cihazdan her veri geldiğinde
      bu verilerin cihazın girdilerini nasıl etkileyeceğini belirler.
  </p>
  <p>
      Kullanıcı kendi betiklerini oluşturabilir ve uygulama ayarlarından bu betiği seçip aktif
      hale getirebilir. Betik oluşturmayla ilgili bilgiler varsayılan dosyaya kod yorumu olarak
      yazılmıştır.
  </p>
  <p>
      Sadece işletim sisteminize özgü olan betikleri kullanın. Betiklerin gücünü çalıştıkları işletim
      sistemindeki sanal aygıt yazılımı sınırlar.
  </p>
`;
