export const title = 'Erişim Reddedildi Hatası (open("/dev/uinput"))';
export const content = `
  <p>
      Sanal aygıtların çalışması için gerekli olan dosyaya erişim izninizin olmadığı anlamına gelir.
      Aşağıdaki kodu çalıştırarak geçici olarak bu problemi çözebilirsiniz. Problemi kalıcı olarak
      çözmek için bir <strong>udev</strong> kuralı eklemeniz gerekebilir.
  </p>
  <code>sudo chmod +0666 /dev/uinput</code>
`;
