export const title = 'Tek Noktaya Yayın Kipinde Bağlanma';
export const content = `
  <ol>
      <li>PC uygulamasında ağ arayüzleri penceresini açın.</li>
      <li>Sağ tarafta bulunan IP adreslerinden birine tıklayarak QR kodu penceresini açın.</li>
      <li>Mobil cihazınızla QR kodunu okutarak IP adresini panoya kopyalayın.</li>
      <li>Mobil uygulamanın ağ ayarlarına girin.</li>
      <li>Hedef adresi panoya kopyaladığınız adrese ayarlayın.</li>
      <li>PC uygulamasının ağ ayarlarına gidin.</li>
      <li>Her iki uygulamada da bağlantı noktalarının aynı olduğundan emin olun.</li>
      <li>
          Bağlama adresini panoya kopyaladığınız adrese ayarlayın (QR kodu gösterildiğinde adres
          otomatik olarak panoya kopyalanmıştı).
      </li>
  </ol>
`;
