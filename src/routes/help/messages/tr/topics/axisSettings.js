export const title = 'Eksen Ayarlarında Başlangıç, Bitiş ve Doğrusallık';
export const content = `
  <p>
      <strong>Başlangıç</strong> ayarı eksenin ölü bölge büyüklüğünü belirler. Eksen sıfır noktasına yeterince
      yakınsa ekseni sıfırlar.
  </p>
  <p>
      <strong>Bitiş</strong> ayarı, doygunluğu belirler. Bu ayarın en faydalı olduğu yer direksiyondur.
      Örneğin direksiyonu daha az döndürerek tam dönüş sağlamak istiyorsanız bu ayarı 100'ün altına
      ayarlamalısınız. 100'ün üstünde bir ayar ise hassasiyeti azaltırken doğrusallığı koruyacaktır ancak bazı
      oyunlarda tam dönüş sağlayamazsınız.
  </p>
  <p>
      <strong>Doğrusallık</strong> ayarı eksenin farklı noktalarda ne kadar çok tepki vereceğini belirler. Örneğin
      50 üstü bir ayarda eksen sıfıra yakınken hassasiyet az olacaktır fakat daha yukarı değerlerde
      hassasiyet artacaktır. 50 altı ayarda ise sıfıra yakın değerlerde hassasiyet yüksek olurken
      yüksek değerlerde hassasiyet azalacaktır.
  </p>
`;
