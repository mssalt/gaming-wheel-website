export const title = 'Takılma ve Gecikme';
export const content = `
  <p>Bunların pek çok sebebi olabilir. Deneyebileceğiniz birkaç şey şunlardır:</p>
  <ul>
      <li>
          Ağ bağlantınız zayıf veya meşgul olabilir. Örneğin başka biri bir şey indiriyor olabilir.
          Bu durumda yapabileceğiniz şeylerden biri mobil cihazınızdan bir hot-spot ağı oluşturup
          onu kullanmaktır.
      </li>
      <li>
          Bluetooth ve WiFi sinyalleri karışıyor olabilir
          (
            <a
              href="https://electronics.stackexchange.com/questions/466798/does-bluetooth-interfere-with-wifi"
              target="_blank"
              rel="noreferrer"
            >
              Does Bluetooth interfere with WiFi?
            </a>
          ).
          Örneğin Bluetooth kulaklığınız telefonunuza bağlıysa direksiyonda takılmalar
          gözlemleyebilirsiniz. Bu durumda Bluetooth kulaklığınızı sadece bilgisayarınıza bağlı tutun.
      </li>
      <li>
          Bazı durumlarda çok noktalı yayın performansı tek noktalı yayına göre daha düşük olabilir.
          Tek noktalı kipte bağlantı için bağlanma başlığındaki talimatlara bakın.
      </li>
      <li>
          Mobil cihazın paket gönderme süresi çok kısa ise bilgisayar tarafında veya ağda düzenli
          tıkanmalara yol açıyor olabilir. Mobil uygulamanın ağ ayarlarından paket gönderme gecikme
          süresini arttırmayı deneyin fakat bunu yaptığınızda gecikme de artacaktır.
      </li>
      <li>
          Yedek paket sayısı orijinal paketle birlikte onun kaç adet kopyasının gönderileceğini
          belirler. Bu sayıyı fazla arttırmak sıkışık bir ağda olumsuz sonuç verebilir. Bu sayıyı
          önce azaltmayı, pozitif sonuç alamadıysanız da arttırmayı deneyin.
      </li>
  </ul>
`;
