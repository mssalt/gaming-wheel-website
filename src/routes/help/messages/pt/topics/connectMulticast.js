export const title = 'Conectando no modo Multicast';
export const content = `
  <ol>
      <li>Insira as configurações de rede do aplicativo móvel.</li>
      <li>Defina o endereço de destino como 234.0.0.55.</li>
      <li>Defina a porta de destino como 41384.</li>
      <li>Vá para as configurações de rede do aplicativo para PC.</li>
      <li>Defina o endereço de ligação como 234.0.0.55.</li>
      <li>Defina a porta como 41384.</li>
      <li>
          A interface preferencial é uma configuração avançada. Se você inserir um nome de
          interface que
          veja na janela <b>Network Interfaces</b> aqui, a conexão é feita apenas naquele
          interface de rede.
      </li>
      <li>
          Enquanto o endereço de destino estiver definido para um endereço no intervalo de
          234.0.0.0 e
          234.255.255.255, será detectado como um endereço multicast.
      </li>
  </ol>
`;
