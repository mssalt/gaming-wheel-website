export const title = 'Erro vcruntime140.dll não encontrado';
export const content = `
<p>Você pode não ter o pacote redistribuível C++ do VS 2017 instalado em seu sistema.</p>
<p>
  Se o seu sistema for <strong>64 bits</strong>, você pode baixá-lo aqui:
  <a
  rel="noreferrer noopener"
  href="https://aka.ms/vs/17/release/vc_redist.x64.exe">
    https://aka.ms/vs/17/release/vc_redist.x64.exe
  </a>
</p>
<p>
  Se o seu sistema for <strong>32 bits</strong>, você pode baixá-lo aqui:
  <a
  rel="noreferrer noopener"
  href="https://aka.ms/vs/17/release/vc_redist.x86.exe">
    https://aka.ms/vs/17/release/vc_redist.x86.exe
  </a>
</p>
`;
