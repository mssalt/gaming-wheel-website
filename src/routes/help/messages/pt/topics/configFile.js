export const title = 'Arquivos de configuração';
export const content = `
  <p>
      Os arquivos de configuração são salvos na linguagem <strong>yaml</strong>. O aplicativo
      salva o
      configurações cada vez que ele fecha. No entanto, se o aplicativo fechar com um erro, as
      configurações
      não são salvos.
  </p>
  <p>
      A localização da pasta de salvamento no Windows é <i>%APPDATA%/gaming_wheel_pc</i>, e o
      a localização no Linux é <i>~/.config/gaming_wheel_pc</i>.
  </p>
`;
