export const title = 'Versão do protocolo de rede';
export const content = `
  <ul>
      <li>Este software usa um protocolo de rede específico para comunicação entre aplicativos
          móveis e de PC.</li>
      <li>É essencial que os aplicativos móveis e de PC usem a mesma versão do protocolo de rede
          para se comunicarem.</li>
      <li>Se as versões do protocolo de rede não corresponderem, a comunicação entre os
          aplicativos <strong>não será possível</strong>.</li>
      <li>Portanto, certifique-se de que os aplicativos para celular e PC usem a mesma versão do
          protocolo de rede para permitir uma comunicação perfeita entre os aplicativos.</li>
  </ul>
`;
