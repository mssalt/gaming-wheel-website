export const title = 'Script de Joystick';
export const content = `
  <p>
      O script é um recurso avançado e requer conhecimento da linguagem de script lua para usá-lo.
  </p>
  <p>
      O aplicativo de computador executa o arquivo <strong>default.lua</strong> na pasta de dados
      do script
      para identificar e gerenciar um dispositivo joystick. Este arquivo identifica o nome,
      interface, fornecedor e
      ID do produto do dispositivo a ser usado, entradas que possui, etc., e determina como os
      dados vindos
      do dispositivo móvel afetará as entradas do dispositivo.
  </p>
  <p>
      O usuário pode criar seus próprios scripts e selecionar e ativar esse script no aplicativo
      configurações. As informações sobre a criação de scripts são gravadas no arquivo padrão como
      um comentário de código.
  </p>
  <p>
      Use apenas scripts específicos para seu sistema operacional. O poder dos scripts é limitado
      pelo software do dispositivo virtual no sistema operacional em que são executados.
  </p>
`;
