export const title = 'Erro de acesso negado (open("/dev/uinput"))';
export const content = `
  <p>
      Isso significa que você não tem permissão para acessar o arquivo, o que é necessário para o
      operação de dispositivos virtuais. Você pode resolver temporariamente esse problema
      executando o
      seguinte código. Pode ser necessário adicionar uma regra <strong>udev</strong> para resolver
      o problema
      permanentemente.
  </p>
  <code>sudo chmod +0666 /dev/uinput</code>
`;
