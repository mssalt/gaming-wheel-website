export const title = 'A conexão cai a cada poucos minutos';
export const content = `
  <p>
    Por padrão, o aplicativo estabelece uma conexão no modo "multicast", mas alguns sistemas
    encerram a comunicação neste modo após um curto período de tempo.
  </p>
  <p>
    Para corrigir esse problema, altere as configurações de conexão para operar no modo “unicast”.
    Você pode ver como fazer isso aqui:
    <a href="#Conexão no modo Unicast">Conexão no modo Unicast</a>
  </p>
`;
