export const title = 'Conexão no modo Unicast';
export const content = `
  <ol>
      <li>Abra a janela de interfaces de rede no aplicativo para PC.</li>
      <li>Clique em um dos endereços IP à direita para abrir a janela do código QR.</li>
      <li>Copie o endereço IP para a área de transferência digitalizando o código QR com seu
          dispositivo móvel.</li>
      <li>Insira as configurações de rede do aplicativo móvel.</li>
      <li>Defina o endereço de destino para aquele que você copiou para a área de transferência.
      </li>
      <li>Vá para as configurações de rede do aplicativo para PC.</li>
      <li>Certifique-se de que as portas sejam as mesmas em ambos os aplicativos.</li>
      <li>
          Defina o endereço de ligação para aquele que você copiou para a área de transferência (o
          endereço foi
          copiado automaticamente para a área de transferência quando o código QR é exibido).
      </li>
  </ol>
`;
