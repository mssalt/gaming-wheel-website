export const title = 'vJoy (Windows)';
export const content = `
  vJoy é um driver de joystick virtual que se apresenta como um joystick real. O aplicativo só
  envia
  comandos do aplicativo móvel para esse driver. Sem este driver, o recurso de joystick não
  trabalhar.
`;
