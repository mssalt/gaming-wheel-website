export const title = 'Início, fim e linearidade nas configurações do eixo';
export const content = `
  <p>
      A configuração <strong>start</strong> determina o tamanho da zona morta do eixo. Se o eixo
      estiver próximo o suficiente
      ao ponto zero, zera o eixo.
  </p>
  <p>
      A configuração <strong>end</strong> determina a saturação. Onde esta configuração é mais
      útil é no
      volante. Por exemplo, se você deseja fornecer rotação completa girando o volante
      menos, você deve definir esta configuração abaixo de 100. Uma configuração acima de 100
      reduzirá a sensibilidade enquanto
      mantendo a linearidade, mas em alguns jogos você não conseguirá atingir a rotação completa.
  </p>
  <p>
      A configuração de <strong>linearidade</strong> determina o quanto o eixo reage em diferentes
      pontos. Por exemplo,
      em um ajuste acima de 50, a sensibilidade será menor quando o eixo estiver próximo de zero,
      mas o
      a sensibilidade aumentará em valores mais altos. Em uma configuração abaixo de 50, a
      sensibilidade será alta
      em valores próximos de zero, enquanto a sensibilidade diminuirá em valores altos.
  </p>
`;
