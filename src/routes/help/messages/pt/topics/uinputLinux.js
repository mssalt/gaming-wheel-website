export const title = 'uinput (Linux)';
export const content = `
  <p>
      uinput é um módulo do kernel para criar dispositivos de entrada virtuais. O aplicativo cria
      virtual
      dispositivos de entrada com este módulo e envia comandos do aplicativo móvel para esses
      dispositivos.
      Sem este módulo, os recursos de joystick, mouse e teclado não funcionarão.
  </p>
  <p>
      Digite o comando abaixo para ativá-lo.
  </p>
  <code>
      $ sudo modprobe uinput
  </code>
`;
