export const title = 'Corrigindo gagueira e lag';
export const content = `
  <p>Pode haver muitas razões para isso. Aqui estão algumas coisas que você pode tentar:</p>
  <ul>
      <li>
          Sua conexão de rede pode estar fraca ou ocupada. Por exemplo, outra pessoa pode estar
          baixando algo. Uma das coisas que você pode fazer neste caso é criar um
          rede de ponto de acesso do seu dispositivo móvel e use-a.
      </li>
      <li>
          Sinais de Bluetooth e WiFi podem estar interferindo
          (
            <a
              href="https://electronics.stackexchange.com/questions/466798/does-bluetooth-interfere-with-wifi"
              target="_blank"
              rel="noreferrer"
            >
              Does Bluetooth interfere with WiFi?
            </a>
          ).
          Por exemplo, se o fone de ouvido Bluetooth estiver conectado ao telefone, você poderá
          observar gagueira no volante. Nesse caso, basta manter o fone de ouvido Bluetooth conectado ao computador.
      </li>
      <li>
          Em alguns casos, o desempenho multicast pode ser inferior ao unicast. Para conexão em
          modo unicast, consulte as instruções no tópico de conexão.
      </li>
      <li>
          Se o tempo de envio de pacotes do dispositivo móvel for muito curto, pode estar causando
          gagueira no lado do computador ou rede. Tente aumentar a latência de envio de pacotes
          nas configurações de rede do aplicativo móvel, mas isso aumentará a latência.
      </li>
      <li>
          O número de pacotes de backup determina quantas cópias do pacote original
          são enviados com ele. Aumentar demais esse número pode ter resultados negativos em
          uma rede congestionada. Tente diminuir esse número primeiro e, se não obtiver resultado
          positivo
          resultados, tente aumentá-lo.
      </li>
  </ul>
`;
