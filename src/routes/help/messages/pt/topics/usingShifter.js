export const title = 'Usando o câmbio';
export const content = `
  <ol>
      <li>Localize o shifter virtual, que tem uma forma circular com números.</li>
      <li>Observe que há um total de 9 marchas (7 + N + R).</li>
      <li>Para mudar para a marcha N, toque no círculo.</li>
      <li>Para mudar para uma marcha com fundo branco, toque no meio do círculo e deslize o dedo
          em direção a ele.</li>
      <li>Para mudar para uma marcha com fundo preto, deslize o dedo na direção dela e deslize
          para trás.</li>
  </ol>
`;
