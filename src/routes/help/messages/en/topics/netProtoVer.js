export const title = 'Network Protocol Version';
export const content = `
  <ul>
    <li>
      This software uses a specific network protocol for communication
      between mobile and PC apps.
    </li>
    <li>
      It is essential that both mobile and PC apps use the same network
      protocol version to communicate with each other.
    </li>
    <li>
      If the network protocol versions do not match, communication between
      the apps <strong>will not be possible</strong>.
    </li>
    <li>
      Therefore, please ensure that both the mobile and PC apps use the same
      network protocol version to enable seamless communication between the
      apps.
    </li>
  </ul>
`;
