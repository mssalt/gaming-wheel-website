export const title = 'Joystick Scripting';
export const content = `
  <p>
    Scripting is an advanced feature and requires knowledge of the lua
    scripting language to use it.
  </p>
  <p>
    The computer application runs the <strong>default.lua</strong> file in
    the script data folder to identify and manage a joystick device. This
    file identifies the name, interface, vendor and product ID of the device
    to be used, inputs it has, etc., and determines how the data coming from
    the mobile device will affect the device's inputs.
  </p>
  <p>
    The user can create their own scripts and select and activate this
    script from the application settings. Information about creating scripts
    is written to the default file as a code comment.
  </p>
  <p>
    Use only scripts that are specific to your operating system. The power
    of scripts is limited by the virtual device software in the operating
    system in which they run.
  </p>
`;
