export const title = "vcruntime140.dll Not Found Error";
export const content = `
<p>You may not have the VS 2017 C++ redistributable package installed on your system.</p>
  <p>
    If your system is <strong>64 bit</strong>, you can download it from here:
    <a
    rel="noreferrer noopener"
    href="https://aka.ms/vs/17/release/vc_redist.x64.exe">
      https://aka.ms/vs/17/release/vc_redist.x64.exe
    </a>
  </p>
  <p>
    If your system is <strong>32 bit</strong>, you can download it from here:
    <a
    rel="noreferrer noopener"
    href="https://aka.ms/vs/17/release/vc_redist.x86.exe">
      https://aka.ms/vs/17/release/vc_redist.x86.exe
    </a>
  </p>
`
