export const title = "Configuration Files";
export const content = `
  <p>
      Configuration files are saved in <strong>yaml</strong> language. The application saves the
      settings each time it closes. However, if the application closes with an error, the settings
      are not saved.
  </p>
  <p>
      The location of the save folder on Windows is <i>%APPDATA%/gaming_wheel_pc</i>, and the
      location on Linux is <i>~/.config/gaming_wheel_pc</i>.
  </p>
`;
