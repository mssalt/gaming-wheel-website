export const title = 'vJoy "There are not enough buttons" Error';
export const content = `
  Start <strong>Configure vJoy</strong> from the start menu, set the number of buttons
  to at least 28 and press the apply button.
`;
