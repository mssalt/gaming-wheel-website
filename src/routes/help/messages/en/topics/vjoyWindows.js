export const title = 'vJoy (Windows)';
export const content = `
  vJoy is a virtual joystick driver that presents itself as a real joystick.
  The app only sends commands from the mobile app to that driver. Without
  this driver, the joystick feature won't work.
`;
