export const title = 'Access Denied Error (open("/dev/uinput"))';
export const content = `
  <p>
    It means that you do not have permission to access the file, which is
    required for the operation of virtual devices. You can temporarily solve
    this problem by running the following code. You may need to add a
    <strong>udev</strong> rule to solve the problem permanently.
  </p>
  <code>sudo chmod +0666 /dev/uinput</code>
`;
