export const title = 'uinput (Linux)';
export const content = `
  <p>
    uinput is a kernel module for creating virtual input devices. The app
    creates virtual input devices with this module and sends commands from
    the mobile app to these devices. Without this module, the joystick,
    mouse, and keyboard features will not work.
  </p>
  <p>Enter the command below to activate it.</p>
  <code>$ sudo modprobe uinput</code>
`;
