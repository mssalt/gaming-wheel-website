export const title = 'Connection Drops Every Few Minutes';
export const content = `
  <p>
    By default, the application establishes a connection in "multicast" mode, but some systems
    terminate communication in this mode after a short time.
  </p>
  <p>
    To fix this problem, change the connection settings to operate in "unicast" mode. You can see
    how to do it here:
    <a href="#Connecting in Unicast Mode">Connecting in Unicast Mode</a>
  </p>
`;
