export const title = "Fixing stuttering and lag";
export const content = `
  <p>
    There can be many reasons for these. Here are a few things you could
    try:
  </p>
  <ul>
    <li>
      Your network connection might be weak or busy. For example, someone
      else might be downloading something. One of the things you can do in
      this case is to create a hot-spot network from your mobile device and
      use it.
    </li>
    <li>
      Bluetooth and WiFi signals may be interfering (
      <a
        href="https://electronics.stackexchange.com/questions/466798/does-bluetooth-interfere-with-wifi"
        target="_blank"
        rel="noreferrer"
      >
        Does Bluetooth interfere with WiFi?
      </a>
      ). For example, if your Bluetooth headset is connected to your phone,
      you can observe stuttering on the steering wheel. In this case, just
      keep your Bluetooth headset connected to your computer.
    </li>
    <li>
      In some cases, multicast performance may be lower than unicast. For
      connection in unicast mode, see the instructions in the connecting
      topic.
    </li>
    <li>
      If the mobile device's packet sending time is too short, it may be
      causing periodic stuttering on the computer side or network. Try
      increasing the packet sending latency in the network settings of the
      mobile app, but doing so will increase the latency.
    </li>
    <li>
      The number of backup packet determines how many copies of the original
      packet are sent with it. Increasing this number too much can have
      negative results in a congested network. Try decreasing this number
      first, and if you don't get positive results, try increasing it.
    </li>
  </ul>
`;
