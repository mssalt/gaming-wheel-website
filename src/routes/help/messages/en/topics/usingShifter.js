export const title = "Using the Shifter";
export const content = `
  <ol>
    <li>
      Locate the virtual shifter, which has a circular shape with numbers on
      it.
    </li>
    <li>Note that there is a total of 9 gears (7 + N + R).</li>
    <li>To shift to the gear N, tap on the circle.</li>
    <li>
      To shift to a gear with a white background, touch the middle of the
      circle and swipe your finger towards it.
    </li>
    <li>
      To shift to a gear with a black background, swipe your finger towards
      it and then swipe back.
    </li>
  </ol>
`;
