export const title = 'Start, End, and Linearity in Axis Settings';
export const content = `
  <p>
    The <strong>start</strong> setting determines the dead zone size of the
    axis. If the axis is close enough to the zero point, it resets the axis.
  </p>
  <p>
    The <strong>end</strong> setting determines the saturation. Where this
    setting is most useful is in the steering wheel. For example, if you
    want to provide full rotation by rotating the steering wheel less, you
    should set this setting below 100. A setting above 100 will reduce
    sensitivity while maintaining linearity, but in some games you won't be
    able to achieve full rotation.
  </p>
  <p>
    The <strong>linearity</strong> setting determines how much the axis
    reacts at different points. For example, at a setting above 50, the
    sensitivity will be less when the axis is close to zero, but the
    sensitivity will increase at higher values. At a sub-50 setting, the
    sensitivity will be high at values close to zero, while the sensitivity
    will decrease at high values.
  </p>
`;
