export const title = 'Connecting in Multicast Mode';
export const content = `
<ol>
    <li>Enter the mobile app's network settings.</li>
    <li>Set the destination address to 234.0.0.55.</li>
    <li>Set destination port to 41384.</li>
    <li>Go to the network settings of the PC application.</li>
    <li>Set the bind address to 234.0.0.55.</li>
    <li>Set port to 41384.</li>
    <li>
        The preferred interface is an advanced setting. If you enter an interface name that you
        see in the <b>Network Interfaces</b> window here, the connection is made only on that
        network interface.
    </li>
    <li>
        As long as the destination address is set to an address in the range of 234.0.0.0 and
        234.255.255.255, it will be detected as a multicast address.
    </li>
</ol>
`;
