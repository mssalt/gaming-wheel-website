export const title = 'Connecting in Unicast Mode';
export const content = `
<ol>
    <li>Open the network interfaces window in the PC application.</li>
    <li>Click one of the IP addresses on the right to open the QR code window.</li>
    <li>Copy the IP address to the clipboard by scanning the QR code with your mobile device.</li>
    <li>Enter the mobile app's network settings.</li>
    <li>Set the destination address to the one you copied to the clipboard.</li>
    <li>Go to the network settings of the PC application.</li>
    <li>Make sure the ports are the same in both apps.</li>
    <li>
        Set the bind address to the one you copied to the clipboard (the address was
        automatically copied to the clipboard when the QR code was shown).
    </li>
</ol>`;
