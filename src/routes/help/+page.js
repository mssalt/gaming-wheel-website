import { getTopics } from './topics';

/** @type {import('./$types').PageLoad} */
export async function load({ data }) {
  const topics = getTopics(data.messages);
  return {
    ...data,
    topics,
  };
}
