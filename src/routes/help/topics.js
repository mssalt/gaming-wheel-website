/**
 * Topic with title and content.
 * @typedef {{ title: string, content: string }} Topic
 */

/** @param {Object.<string ,string>} messages */
export function getTopics(messages) {
  return [
    {
      title: messages.topicConnectMulticastTitle,
      content: messages.topicConnectMulticastContent,
    },
    {
      title: messages.topicConnectUnicastTitle,
      content: messages.topicConnectUnicastContent,
    },
    {
      title: messages.topicKeepsDisconnTitle,
      content: messages.topicKeepsDisconnContent,
    },
    {
      title: messages.topicNotEnoughButtonsTitle,
      content: messages.topicNotEnoughButtonsContent,
    },
    {
      title: messages.topicVcr140NotFoundTitle,
      content: messages.topicVcr140NotFoundContent,
    },
    {
      title: messages.topicConfigFilesTitle,
      content: messages.topicConfigFilesContent,
    },
    {
      title: messages.topicUsingShifterTitle,
      content: messages.topicUsingShifterContent,
    },
    {
      title: messages.topicFixLagTitle,
      content: messages.topicFixLagContent,
    },
    {
      title: messages.topicUinputDeniedTitle,
      content: messages.topicUinputDeniedContent,
    },
    {
      title: messages.topicVjoyWindowsTitle,
      content: messages.topicVjoyWindowsContent,
    },
    {
      title: messages.topicUinputLinuxTitle,
      content: messages.topicUinputLinuxContent,
    },
    {
      title: messages.topicAxisSettingsTitle,
      content: messages.topicAxisSettingsContent,
    },
    {
      title: messages.topicNetProtoVerTitle,
      content: messages.topicNetProtoVerContent,
    },
    {
      title: messages.topicJoyScriptTitle,
      content: messages.topicJoyScriptContent,
    },
  ];
}
