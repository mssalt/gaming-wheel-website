/** @param {Headers} headers */
export function getUserLang(headers) {
  const langFull = headers.get('accept-language');
  return langFull ? langFull.split(',')[0].split('-')[0] : 'en';
}

/** 
  * @param {string} input
  * @param {Object.<string, string>} vars */
export function transWithVars(input, vars) {
  let text = input;
  Object.keys(vars).map((k) => {
    const regex = new RegExp(`{{${k}}}`, 'g');
    text = text.replace(regex, vars[k]);
  });
  return text;
}
